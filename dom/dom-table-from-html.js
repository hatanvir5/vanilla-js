"use strict";

let addresses = [
  {
    name: "Tanvir Ahmad",
    home: "Najir Ahmad Master Bari",
    village: "Norsinghapur",
    post_Office: "Kabir Hat",
    thana: "Kabir Hat",
    district: "Noakhali"
  },
  {
    name: "Biplob Ahmad",
    home: "Kazi bari",
    village: "Madla",
    post_Office: "Basurhat",
    thana: "Companigonj",
    district: "Noakhali"
  }
];

let container = document.getElementById("container");


for (let address of addresses) {
  let tr = "";
  tr += "<tr>";
  tr += "<td>" + address.name + "</td>";
  tr += "<td>" + address.home + "</td>";
  tr += "<td>" + address.village + "</td>";
  tr += "<td>" + address.post_Office + "</td>";
  tr += "<td>" + address.thana + "</td>";
  tr += "<td>" + address.district + "</td>";
  tr += "</tr>";
  container.innerHTML += tr;
}
