'use strict'
// Create table by createElement
let data = (function() {
  let addresses = [
    {
      name: "Tanvir Ahmad",
      home: "Najir Ahmad Master Bari",
      village: "Norsinghapur",
      post_Office: "Kabir Hat",
      thana: "Kabir Hat",
      district: "Noakhali"
    },
    {
      name: "Biplob Ahmad",
      home: "Kazi bari",
      village: "Madla",
      post_Office: "Basurhat",
      thana: "Companigonj",
      district: "Noakhali"
    }
  ];
  return addresses;
})();


let table = window.document.createElement("table");

for (let address of data) {



 let cellId = document.createTextNode(address.name);
 let  cellIdTd = document.createElement("td");
  cellIdTd.appendChild(cellId);


  let cellHome= document.createTextNode(address.home);
  let cellHomeTd = document.createElement("td");
  cellHomeTd.appendChild(cellHome);

  let cellVillage= document.createTextNode(address.village);
 let  cellVillageTd = document.createElement("td");
  cellVillageTd.appendChild(cellVillage);

  let cellPostOffice= document.createTextNode(address.post_office)
  let cellPostOfficeTd = document.createElement("td");
  cellPostOfficeTd.appendChild(cellPostOffice);

 let cellThana = document.createTextNode(address.thana);
  let cellThanaTd = document.createElement("td");
  cellThanaTd.appendChild(cellThana);

  let cellDistrict= document.createTextNode(address.district);
  let cellDistrictTd = document.createElement("td");
  cellDistrictTd.appendChild(cellDistrict);


  let cellTr =document.createElement('tr')
  cellTr.appendChild(cellIdTd)
  cellTr.appendChild(cellHomeTd)
  cellTr.appendChild(cellVillageTd)
  cellTr.appendChild(cellPostOfficeTd)
  cellTr.appendChild(cellThanaTd)
  cellTr.appendChild(cellDistrictTd)

  table.appendChild(cellTr)
  
}
table.border=1
let rootPath=document.querySelector('#root')
rootPath.appendChild(table)
console.log(table)
