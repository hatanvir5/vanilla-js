"use strict";
// Create table by createElement
let data = (function() {
  let addresses = [
    {
      name: "Tanvir Ahmad",
      home: "Najir Ahmad Master Bari",
      village: "Norsinghapur",
      post_Office: "Kabir Hat",
      thana: "Kabir Hat",
      district: "Noakhali"
    },
    {
      name: "Biplob Ahmad",
      home: "Kazi bari",
      village: "Madla",
      post_Office: "Basurhat",
      thana: "Companigonj",
      district: "Noakhali"
    }
  ];

  return addresses;
 
})();

let table = document.createElement("table");
let tr = document.createElement("tr");
let thNameCell = document.createTextNode("Name");
let thName = document.createElement("th");
thName.appendChild(thNameCell);
tr.appendChild(thName);

let thHomeCell = document.createTextNode("Home");
let thHome = document.createElement("th");
thHome.appendChild(thHomeCell);
tr.appendChild(thHome);

let thVillageCell = document.createTextNode("Village");
let thVillage = document.createElement("th");
thVillage.appendChild(thVillageCell);
tr.appendChild(thVillage);

let thPostOfficeCell = document.createTextNode("Post Office");
let thPostOffice = document.createElement("th");
thPostOffice.appendChild(thPostOfficeCell);
tr.appendChild(thPostOffice);

let thThanaCell = document.createTextNode("Thana");
let thThana = document.createElement("th");
thThana.appendChild(thThanaCell);
tr.appendChild(thThana);

let thDistrictCell = document.createTextNode("District");
let thDistrict = document.createElement("th");
thDistrict.appendChild(thDistrictCell);
tr.appendChild(thDistrict);
table.appendChild(tr);








for (let address of data) {
  let cellNameTd = document.createElement("td");
  let cellName = document.createTextNode(address.name);
  
  cellNameTd.appendChild(cellName);

  let cellHome = document.createTextNode(address.home);
  let cellHomeTd = document.createElement("td");
  cellHomeTd.appendChild(cellHome);

  let cellVillage = document.createTextNode(address.village);
  let cellVillageTd = document.createElement("td");
  cellVillageTd.appendChild(cellVillage);

  let cellPostOffice = document.createTextNode(address.post_Office);
  let cellPostOfficeTd = document.createElement("td");
  cellPostOfficeTd.appendChild(cellPostOffice);

  let cellThana = document.createTextNode(address.thana);
  let cellThanaTd = document.createElement("td");
  cellThanaTd.appendChild(cellThana);

  let cellDistrict = document.createTextNode(address.district);
  let cellDistrictTd = document.createElement("td");
  cellDistrictTd.appendChild(cellDistrict);

  let cellTr = document.createElement("tr");


  cellTr.appendChild(cellNameTd);
  cellTr.appendChild(cellHomeTd);
  cellTr.appendChild(cellVillageTd);
  cellTr.appendChild(cellPostOfficeTd);
  cellTr.appendChild(cellThanaTd);
  cellTr.appendChild(cellDistrictTd);

  table.appendChild(cellTr);
}
//table.border = 1;
table.setAttribute('border',1)



let rootPath = document.querySelector("#root");
//let rootPath=document.getElementById("root")

 rootPath.appendChild(table);
 console.log(table);
