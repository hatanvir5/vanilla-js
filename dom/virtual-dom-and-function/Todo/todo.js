const Tanvir={}
Tanvir.TodoList={
    getTodoId:function(id){
        let todoIdContainer=document.createElement('h3')
        let todoIdTextnode=document.createTextNode("ID:"+id)
       todoIdContainer.appendChild(todoIdTextnode)
       return todoIdContainer
    },
    getTodoTitle:function(title){
        let todoTitleContainer=document.createElement('h3')
        let todoTitleTextnode=document.createTextNode("Title:"+title)
       todoTitleContainer.appendChild(todoTitleTextnode)
       return todoTitleContainer
    },
    getTodoStatus:function(completed){
        let todoStatusContainer=document.createElement('h3')
        let todoStatusTextnode=document.createTextNode("Completed:"+completed)
       todoStatusContainer.appendChild(todoStatusTextnode)
       return todoStatusContainer
    },
    getTodoList:function(todosData){
        let container=document.createElement('div')
        let todoId=this.getTodoId(todosData.id)
        let todoTitle=this.getTodoTitle(todosData.title)
        let todoStatus=this.getTodoStatus(todosData.completed)
        container.appendChild(todoId)
        container.appendChild(todoTitle)
        container.appendChild(todoStatus)
    
        let fieldset=document.createElement('fieldset')
        fieldset.appendChild(container)
        return fieldset;
        return container

},
display:function(location, todoslist){
let htmlLocation=document.querySelector(location)
for( let todosData of todoslist){
    let todos=this.getTodoList(todosData)
    htmlLocation.appendChild(todos)
   
}
}};

function loadData() {
    fetch('https://jsonplaceholder.typicode.com/todos')
        .then(response => response.json())
        .then(function (todos) {
            Tanvir.TodoList.display('#root', todos);
        })

}