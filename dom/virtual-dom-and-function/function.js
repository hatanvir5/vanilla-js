function tdata(column) {
  let tdCell = document.createTextNode(column);
  let td = document.createElement("td");
  td.appendChild(tdCell);
  return td;
}



function tBodyrow(rows) {
  let tr = document.createElement("tr");
  let tableData = tdata();
  tr.appendChild(tableData);
  return tr;
}

function thead(columns) {
  let theadText = document.createTextNode(columns);
  let th = document.createElement("th");
  th.appendChild(theadText);

  return th;
}

function tHeadRow() {
  let headTr = document.createElement("tr");
  let th = thead();
  headTr.appendChild(th);
  return headTr;
}


function createTable(collection) {
  let table = document.createElement("table");
  let thead = tHeadRow();
  let tbody = tBodyrow();
  table.appendChild(thead);
  table.appendChild(tbody);
  
  return table
}

function createTablebyCollection(collection, locatin) {
  let table = createTable(collection);
  let container = document.querySelector("location");
  container.appendChild(table);

  return true

}


console.log(createTablebyCollection())





