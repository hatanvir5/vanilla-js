"use strict";

const Tanvir = {};
Tanvir.htmlTable = {
 
  imageContainer: function(avatar_url){

    let img = document.createElement("img");
    img.src=countries.avatar_url
  tr.appendChild(img)
  },
    


  buildTh: function(column) {
    let theadText = document.createTextNode(column[0].toUpperCase()+column.slice(1));
    let th = document.createElement("th");
    th.appendChild(theadText);
    return th;
  },

  buildTableHead: function(titles) {
    let tr = document.createElement("tr");
    
    for (let title of titles) {
      let th = this.buildTh(title);
      tr.appendChild(th);
    }
    return tr;
  },

  buildTbody: function(column) {
    let tdCell = document.createTextNode(column);
    let td = document.createElement("td");
    td.appendChild(tdCell);
    return td;
  },

  buildTableBody: function(collection) {
 
    let tbody = document.createElement('TBODY');
    for (let data of collection) {
      let tr=document.createElement("tr");
      let td = "";
      for (let proparty in data) {

        td = this.buildTbody(data[proparty]);
        tr.appendChild(td);
     
      }
     
      tbody.appendChild(tr);
      
    }
   
    return tbody;
  },

  buildTable: function(collection) {

    let table = document.createElement("table");
    let titles = Object.keys(collection[0]);
    let tHead = this.buildTableHead(titles);
    let tBody = this.buildTableBody(collection);
    table.appendChild(tHead);
    table.appendChild(tBody);
    // table.border = 1; // trivial way
    table.setAttribute('border', '1'); // Nodes discussion
    
    return table;
  },

  display: function( data, domlocation) {
    
    let container = document.querySelector(domlocation);
    let table = this.buildTable(data);
    container.appendChild(table);
    return true;
  }

};

console.log(imageContainer())