"use strict";

const addressInfo = (function () {
  let addresses = [{
      name: "Tanvir Ahmad",
      home: "Najir Ahmad Master Bari",
      village: "Norsinghapur",
      "post Office": "Kabir Hat",
      thana: "Kabir Hat",
      district: "Noakhali"
    },
    {
      name: "Biplob Ahmad",
      home: "Kazi bari",
      village: "Madla",
      "post Office": "Basurhat",
      thana: "Companigonj",
      district: "Noakhali"
    },
    {
      name: "Sirajul Islam",
      home: "Master Bari",
      village: "Haijdi",
      "post Office": "Chaprasirhat",
      thana: "Kabirhat",
      district: "Noakhali"
    }
  ];
  return addresses;
})();

let headFunc = function (headText) {
  let headContent = document.createTextNode(headText);
  let head = document.createElement("th");
  head.appendChild(headContent);
  return head;
};

let dataFunc = function (dataText) {
  let dataContent = document.createTextNode(dataText);
  let cell = document.createElement("td");
  cell.appendChild(dataContent);
  return cell;
};

let table = document.createElement("table");
table.border = 1;

let tr = document.createElement("tr");

for (let property in addressInfo[0]) {
  tr.appendChild(headFunc(property.toUpperCase()));
}

for(let tuli of addressInfo){
  console.log(tuli)
}



//  for (let data of addressInfo) {
//   let td = "";
  
//    let tr = document.createElement("tr");
//    for (let property in data) {
   
//     td = dataFunc(data[property]);

   
//     tr.appendChild(td);

//     console.log("tr="+tr)
//   }

//   table.appendChild(tr);
// }

// let container = document.querySelector("#root");
// container.appendChild(table);
