'use strict'
const Tanvir = {}
Tanvir.Comment = {

    getCommentName: function (name) {
        let commentNameContainer = document.createElement('span')
        let commentNameTextnode = document.createTextNode("Name:" + name + "," + " ")
        commentNameContainer.appendChild(commentNameTextnode)
        return commentNameContainer
    },

    getCommentEmail: function (email) {
        let commentEmailContainer = document.createElement('strong')
        let commentEmailTextnode = document.createTextNode("Email: " + email + "," + " ")
        commentEmailContainer.appendChild(commentEmailTextnode)
        return commentEmailContainer
    },


    getCommentBody: function (body) {
        let commentBodyContainer = document.createElement('span')
        let commentBodyTextnode = document.createTextNode("Comment: " + body + ".")
        commentBodyContainer.appendChild(commentBodyTextnode)
        return commentBodyContainer
    },

    getComment: function (commentData) {
        let commentContainer = document.createElement('p')
        let CommentName = this.getCommentName(commentData.name)
        let CommentEmail = this.getCommentEmail(commentData.email)
        let CommentBody = this.getCommentBody(commentData.body)

        commentContainer.appendChild(CommentName)
        commentContainer.appendChild(CommentEmail)
        commentContainer.appendChild(CommentBody)
        let fieldset=document.createElement('fieldset')
        fieldset.appendChild(commentContainer)
        return fieldset;

    },

    display: function (location, commentsData) {
        let container = document.querySelector(location)
        for (let commentData of commentsData) {
            let comment = this.getComment(commentData)
            container.appendChild(comment)
        }
    }

}



   function loadData(){
        fetch('https://jsonplaceholder.typicode.com/comments')
        .then(response => response.json())
        .then(function (comments) {
            Tanvir.Comment.display('#root', comments);
        })

    }

