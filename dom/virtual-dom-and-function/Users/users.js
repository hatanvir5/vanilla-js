'use strict'
const Tanvir = {}
Tanvir.Users = {

    getName: function (name) {
        let nameContainer = document.createElement('p')
        let nameTextnode = document.createTextNode("Name:" + name + ",")
        nameContainer.appendChild(nameTextnode)
        return nameContainer
    },
    getUserName: function (username) {
        let userNameContainer = document.createElement('p')
        let userNameTextnode = document.createTextNode("Username: " + username + ",")
        userNameContainer.appendChild(userNameTextnode)
        return userNameContainer
    },

    getEmail: function (email) {
        let emailContainer = document.createElement('p')
        let emailTextnode = document.createTextNode("Email:" + email + ",")
        emailContainer.appendChild(emailTextnode)
        return emailContainer
    },

    getAddress: function (address) {
        let addressContainer = document.createElement('p')
        let addressTextnode = document.createTextNode("Address:")
        addressContainer.appendChild(addressTextnode)
        addressContainer.appendChild(this.getStreet(address.street))
        addressContainer.appendChild(this.getSuite(address.suite))
        addressContainer.appendChild(this.getCity(address.city))
        addressContainer.appendChild(this.getZipcode(address.zipcode))
        addressContainer.appendChild(this.getGeo(address.geo))
        return addressContainer
    },
    getStreet: function (street) {
        let streetContainer = document.createElement('span')
        let streetTextnode = document.createTextNode("Street:" + street + "," + " ")
        streetContainer.appendChild(streetTextnode)
        return streetContainer
    },
    getSuite: function (suite) {
        let suiteContainer = document.createElement('span')
        let suiteTextnode = document.createTextNode("Suite:" + suite + "," + " ")
        suiteContainer.appendChild(suiteTextnode)
        return suiteContainer
    },
    getCity: function (city) {
        let cityContainer = document.createElement('span')
        let cityTextnode = document.createTextNode("City:" + city + "," + " ")
        cityContainer.appendChild(cityTextnode)
        return cityContainer
    },
    getZipcode: function (zipcode) {
        let zipcodeContainer = document.createElement('span')
        let zipcodeTextnode = document.createTextNode("Zipcode:" + zipcode + "," + " ")
        zipcodeContainer.appendChild(zipcodeTextnode)
        return zipcodeContainer
    },
    getGeo: function (geo) {
        let geoContainer = document.createElement('P')
        let geoTextnode = document.createTextNode("Geo: ")
        geoContainer.appendChild(geoTextnode)
        geoContainer.appendChild(this.getLat(geo.lat))
        geoContainer.appendChild(this.getLng(geo.lng))
        return geoContainer
    },
    getLat: function (lat) {
        let latContainer = document.createElement('span')
        let latTextnode = document.createTextNode("Lat:" + lat + "," + " ")
        latContainer.appendChild(latTextnode)
        return latContainer
    },
    getLng: function (lng) {
        let lngContainer = document.createElement('span')
        let lngTextnode = document.createTextNode("Lng:" + lng)
        lngContainer.appendChild(lngTextnode)
        return lngContainer
    },
    getPhone: function (phone) {
        let phoneContainer = document.createElement('p')
        let phoneTextnode = document.createTextNode("Phone Numbera:" + " " + phone)
        phoneContainer.appendChild(phoneTextnode)
        return phoneContainer
    },
    getWebsite: function (website) {
        let websiteContainer = document.createElement('p')
        let websiteTextnode = document.createTextNode("Website:" + " " + website)
        websiteContainer.appendChild(websiteTextnode)
        return websiteContainer
    },
    getCompany: function (company) {
        let companyContainer = document.createElement('p')
        let companyTextnode = document.createTextNode("Company:" + " ")
        companyContainer.appendChild(companyTextnode)
        companyContainer.appendChild(this.getCompanyName(company.name))
        companyContainer.appendChild(this.getCatchhrase(company.catchPhrase))
        companyContainer.appendChild(this.getBS(company.bs))
        return companyContainer
    },
    getCompanyName: function (name) {
        let companyNameContainer = document.createElement('span')
        let companyNameTextnode = document.createTextNode("Name:" + name + "," + " ")
        companyNameContainer.appendChild(companyNameTextnode)
        return companyNameContainer
    },
    getCatchhrase: function (catchPhrase) {
        let catchPhraseContainer = document.createElement('span')
        let catchPhraseTextnode = document.createTextNode("CatchPhrase:" + catchPhrase + "," + " ")
        catchPhraseContainer.appendChild(catchPhraseTextnode)
        return catchPhraseContainer
    },
    getBS: function (bs) {
        let bsContainer = document.createElement('span')
        let bsTextnode = document.createTextNode("BS:" + bs + ".")
        bsContainer.appendChild(bsTextnode)
        return bsContainer
    },

    getUsers: function (userData) {
        let container = document.createElement('div')
        let names = this.getName(userData.name)
        let usersname = this.getUserName(userData.username)
        let emails = this.getEmail(userData.email)
        let addresses = this.getAddress(userData.address)
        let phones = this.getPhone(userData.phone)
        let websites = this.getWebsite(userData.website)
        let companies = this.getCompany(userData.company)

        container.appendChild(names)
        container.appendChild(usersname)
        container.appendChild(emails)
        container.appendChild(addresses)
        container.appendChild(phones)

        container.appendChild(websites)
        container.appendChild(companies)
        let fieldset = document.createElement('fieldset')
        fieldset.appendChild(container)
        return fieldset

    },
    display: function (location, usersData) {
        let htmlLocation = document.querySelector(location)
        for (let userData of usersData) {
            let user = this.getUsers(userData)
            htmlLocation.appendChild(user)
        }
    }

}

function loadData() {
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(function (users) {
            Tanvir.Users.display('#root', users);
        })

}