"use strict";

let data = (function() {
  let addresses = [
    {
      name: "Tanvir Ahmad",
      home: "Najir Ahmad Master Bari",
      village: "Norsinghapur",
      post_Office: "Kabir Hat",
      thana: "Kabir Hat",
      district: "Noakhali"
    },
    {
      name: "Biplob Ahmad",
      home: "Kazi bari",
      village: "Madla",
      post_Office: "Basurhat",
      thana: "Companigonj",
      district: "Noakhali"
    },
    {
      name: "Sirajul Islam",
      home: "Master Bari",
      village: "Haijdi",
      post_Office: "Chaprasirhat",
      thana: "Kabirhat",
      district: "Noakhali"
    }
  ];
  return addresses;
})();

let table = document.createElement("table");
let tr = document.createElement("tr");

let createTableCell = function(cellText) {
  let cellContainer = document.createElement("th");
  let cellContent = document.createTextNode(cellText);
  cellContainer.appendChild(cellContent);

  return cellContainer;
};

tr.appendChild(createTableCell("Name"));
tr.appendChild(createTableCell("Home"));
tr.appendChild(createTableCell("Village"));
tr.appendChild(createTableCell("Post Office"));
tr.appendChild(createTableCell("Thana"));
tr.appendChild(createTableCell("District"));
table.appendChild(tr);

let createDataCell = function(dataText) {
  let dataContent = document.createTextNode(dataText);
  let dataContainer = document.createElement("td");
  dataContainer.appendChild(dataContent);
  return dataContainer;
};

for (let addressInfo of data) {
  //     for(let propertice in addressInfo){
  // console.log(propertice)
  //     }

  let trForData = document.createElement("tr");

  let dataName = createDataCell(addressInfo.name);
  trForData.appendChild(dataName);

  let dataHome = createDataCell(addressInfo.home);
  trForData.appendChild(dataHome);

  let dataVillage = createDataCell(addressInfo.village);
  trForData.appendChild(dataVillage);

  let dataPostOffice = createDataCell(addressInfo.post_Office);
  trForData.appendChild(dataPostOffice);

  let dataThana = createDataCell(addressInfo.thana);
  trForData.appendChild(dataThana);

  let dataDistrict = createDataCell(addressInfo.district);
  trForData.appendChild(dataDistrict);

  table.appendChild(trForData);
}

let tableContainer = document.querySelector("#root");
tableContainer.appendChild(table);
table.border = 1;
