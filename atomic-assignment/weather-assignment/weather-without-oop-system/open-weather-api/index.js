window.addEventListener('load', () => {

    let long;
    let lat;
    let countryLocation = document.querySelector('.country-location')
    let weatherIcon=document.querySelector('.weather-icon')
    let temperetureDegree = document.querySelector('.tempereture-degree')
     let minTemperetureDegree=document.querySelector('.min-tempereture-degree')
     let maxTemperetureDegree=document.querySelector('.max-tempereture-degree')
    let locationDescription = document.querySelector('.location-description')
    let temperetureDescription = document.querySelector('.tempereture-description')

    const KELVIN = 273;

    const apiKey	 = "de5d66b454fa6bd2e68b9c40aa2b36f8";


    if (navigator.geolocation) {

        navigator.geolocation.getCurrentPosition(position => {
            long = position.coords.longitude;
            lat = position.coords.latitude;
         
              const apiLink = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${apiKey}`;

            

             async function getWeather() {
                try {
            
                    let responce = await fetch(apiLink)
                    let data = await responce.json()
                    return data;
                } catch (err) {
                    console.log(err)
                }
            
            }
            getWeather()
                 .then(data =>{
                      console.log(data)
            
                    temperetureDegree.textContent = `Tempereture:  ${parseInt(data.main.temp - KELVIN)} ° C`;
                    locationDescription.textContent = `Location:  ${data.name}`;
                    countryLocation.textContent = `Country: ${data.sys.country}`;
                    weatherIcon.textContent=`Weather icon:  ${data.weather[0].icon}`
                    temperetureDescription.textContent = `Weather Type: ${data.weather[0].description}`;
                    minTemperetureDegree.textContent=`Minimum Temperature: ${data.main.temp_min} ° C`;
                    maxTemperetureDegree.innerHTML=`Maximum Temperature: ${data.main.temp_max}  ° C`;
                   
                 

               })
        })
    }

})
