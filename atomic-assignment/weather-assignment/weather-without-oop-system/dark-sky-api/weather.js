window.addEventListener('load', () => {

    let long;
    let lat;
    let countryLocation = document.querySelector('.country-location')
    //let weatherIcon=document.querySelector('.weather-icon')
    let temperetureDegree = document.querySelector('.tempereture-degree')
    // let minTemperetureDegree=document.querySelector('.min-tempereture-degree')
    // let maxTemperetureDegree=document.querySelector('.max-tempereture-degree')
    let locationDescription = document.querySelector('.location-description')
    let temperetureDescription = document.querySelector('.tempereture-description')

    const KELVIN = 273;



    // const key = "82005d27a116c2880c8f0fcb866998a0";
    const apiKey	 = "de5d66b454fa6bd2e68b9c40aa2b36f8";
    // const tanvirsapikey="47ed4784a95831e542b3a59b2d453fb0";

    if (navigator.geolocation) {

        navigator.geolocation.getCurrentPosition(position => {
            long = position.coords.longitude;
            lat = position.coords.latitude;
            // const proxy = "http://cors-anywhere.herokuapp.com/"
            // const api = `${proxy}http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${apiKey	}`;

            // const proxy="http://cors-anywhere.herokuapp.com/"
              const api = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${apiKey}`;
            //  const api= `https://api.darksky.net/forecast/fd9d9c6418c23d94745b836767721ad1/${lat},${long}`
            

            fetch(api)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    console.log(data)

                    temperetureDegree.textContent = "Tempereture:" + parseInt(data.main.temp - KELVIN);
                    locationDescription.textContent = "Location:" + data.name
                    countryLocation.textContent = "Country:" + data.sys.country
                    temperetureDescription.textContent = "Weather Type:" + data.weather[0].description
                })
        })
    }

})
