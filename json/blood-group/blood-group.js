"use strict";
const blood_group_set = [
  {
    name: "Tanvir Ahmad",
    blood_group: "A(+)ve",
    donate_time: "7 times",
    last_donate_date: "24-11-19"
  },
  {
    name: "Jahid Hossoin Nayem",
    blood_group: "AB(+)ve",
    donate_time: "3 times",
    last_donate_date: "24-11-19"
  },
  {
    name: "Samiul Islam",
    blood_group: "B(+)ve",
    donate_time: "6 times",
    last_donate_date: "10-10-19"
  }
];


const containerData = document.getElementById("tableContent");
for (let bloodGroups of blood_group_set) {
  let container = "";

  container += "<tr>";
  container += "<td>" + bloodGroups.name + "</td>";
  container += "<td>" + bloodGroups.blood_group + "</td>";
  container += "<td>" + bloodGroups.donate_time + "</td>";
  container += "<td>" + bloodGroups.last_donate_date + "</td>";
  container += "</tr>";
  containerData.innerHTML += container;
}
