const buses=[
    {
        name:"Hanif",
        root:"Dhaka to CTG"
    },
    {
        name:"Shyamoli",
        root:"Ctg to Bogura"
    },
    {
        name:"Star Line",
        root:"Ctg to Feni"
    },
    {
        name:"Basurhat Express", 
        root:"Ctg to Noakhali"
    },
    {
        name:"Soudia",
        root:"Dhaka to Cox's Bazar"
    },
    {
        name:"Dream Line",
        root:"Noakhali to Dhaka"
    }
]