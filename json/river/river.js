let rivers = [
  {
    river_name: "Surma-Meghna",
    location: "	Sylhet (180), Comilla (146), Barisal (90)",
    length: "	359 miles (578 km)"
  },
  {
    river_name: "Karatoya-Atrai-Gurgumari-Hursagar",
    location: "Dinajpur (161), Rajshahi (160) & Pabna (50)",
    length: "382 miles (615 km)"
  },
  {
    river_name: "Padma (Ganges)",
    location: "Rajshahi (90) Pabna (60) Dhaka (60) & Faridpur (80)",
    length: "222 miles (357 km)"
  },
  {
    river_name: "Garai-Madhumati-Baleswar",
    location:
      "	Kushtia (36) Faridpur (70) Jessore (91) Khulna (104) and Barisal (65)",
    length: "233 miles (375 km)"
  },
  {
    river_name: "Old Brahmaputra",
    location: "Mymensingh (172)",
    length: "	150 miles (240 km"
  }
];
