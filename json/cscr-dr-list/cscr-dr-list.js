const CSCR_Doctors_Information = [{

    Name: "Prof. (Dr.) Mahbub Kamal Chowdhury",
    Qualification: "MBBS, FCPS(Medicine)",
    Visiting_Hour: "11:00AM–2:00PM",
    Room_No: 305
},
{
    Name: "Prof. (Dr.) M.A.Faiz",
    Qualification: "MBBS, FCPS (Medicine), FRCP (EDIN), PHD (U.K)",
    Visiting_Hour: "!1:00AM-1:00PM",
    Room_No: 405,
},
{
    Name: "Prof. (Dr.) Faridul Islam",
    Qualification: "MBBS, FCPS(Medicine)",
    Visiting_Hour: "5: 00PM - 11: 00PM",
    Room_No: 409,
},
{
    Name: "Prof. (Dr.) Md.Gofranul Hoque",
    Qualification: "MBBS, FCPS(Medicine)",
    Visiting_Hour: "6: 30PM - 11: 00PM",
    Room_No: 406,
},
{
    Name: "Dr.Md.Amir Hossain",
    Qualification: "MBBS, FCPS(Medicine)",
    Visiting_Hour: "8: 00PM - 11: 00PM",
    Room_No: 207,
},
{
    Name: "Dr.Anisul Awal",
    Qualification: "MBBS, MCPS, FCPS(Medicine)",
    Visiting_Hour: "9: 30PM - 11: 30PM",
    Room_No: 312,
},
{
    Name: "Dr.Mehrunnissa Khanom",
    Qualification: "MBBS(Gold Medalist), MRCP(UK)",
    Visiting_Hour: "9: 00AM - 12: 30PM",
    Room_No: 414,
}]