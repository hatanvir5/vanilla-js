const pet_data=

[
    {
        "name": "Kitty",
        "species": "cat",
        "likes": "fresh food",
        "dislikes": "stale food"

    },
    {
        "name": "Pupster",
        "species": "dog",
        "likes": "tomatoes, peas",
        "dislikes": "bread"
    },
    {
        "name": "Tux",
        "species": "cat",
        "likes": "fancy dishes",
        "dislikes": "basic cat food"

    }
];
