const vehicles = [{
        vehicle_name: "aircraft",
        description: "any machine capable of flying by means of buoyancy or aerodynamic forces, such as a glider, helicopter, or aeroplane"
    },
    {
        vehicle_name: "ambulance",
        description: "a motor vehicle designed to carry sick or injured people"
    },
    {
        vehicle_name: "articulated lorry or (informal) artic",
        description: "a lorry made in two separate sections, a tractor and a trailer, connected by a pivoted bar"
    },
    {
        vehicle_name: "autocycle",
        description: "a bicycle powered or assisted by a small engine"
    },
    {
        vehicle_name: "autorickshaw",
        description: "a light three-wheeled vehicle driven by a motorcycle engine"
    },
    {
        vehicle_name: "barrow",
        description: "a flat tray for transporting loads, usually carried by two people"
    },
    {
        vehicle_name: "boat",
        description: "a small vessel propelled by oars, paddle, sails, or motor for travelling, transporting goods, etc, esp one that can be carried aboard a larger vessel"
    },
    {
        vehicle_name: "bus",
        description: "a large motor vehicle designed to carry passengers between stopping places along a regular route"
    },
]