const schools=[
    {
        school_name:"Abdus Sobhan Rahat Ali High School",
        location:"Patiya, Chittagong",
        established:"	1914"
    },
    {
        school_name:"Fatickchari Coronation Model High School",
        location:"Fatickchari, Chittagong District",
        established:"	1912"
    },
    {
        school_name:"Bhujpur Model High School",
        location:"Bhujpur, Chittagong",
        established:"1946"
    },
    {
        school_name:"Abdul Khalek Academy",
        location:"	Sandwip, Chittagong",
        established:"1930"
    },
    {
        school_name:"Abdur Rahman Government Girls' High School",
        location:"Patiya, Chittagong",
        established:"1957"
    },
    {
        school_name:"Agrabad Balika Bidyalay",
        location:"	Agrabad, Chittagong",
        established:"	1981"
    },
]