<!DOCTYPE html>
<html>
<body>

<?php
echo "<h1>Hello World</h1>";

echo "Escaping from html"
?>
<p>This is going to be ignored by PHP and displayed by the browser.</p>
<?php echo 'While this is going to be parsed.'; ?>
<p>This will also be ignored by PHP and displayed by the browser.</p>
<?php
$num_1=5;
$num_2=10;

if(num_1==num_2){
    echo $num_1.$num_2;
}
else{
    echo $num_1+$num_2;
}

?>
</body>
</html>